const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href').substr(1)
    
    document.getElementById(blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}

// jQuery(function(f){
//   let element = f('#up');
//   f(window).scroll(function(){
//       element['fade'+ (f(this).scrollTop() > 300 ? 'In': 'Out')](400);           
//   });
// });

